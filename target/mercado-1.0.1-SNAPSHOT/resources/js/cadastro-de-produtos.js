const app = new Vue({
    el: '#app',
    data: {
        errors: [],
        nome: null,
        vol: null,
        uni: null,
        est: null,
        checado: null
    },
    methods: {
        checkForm: function (e) {

            if (this.nome && this.vol && this.uni && this.checado && this.est) {
                axios({
                        method: 'post', // verbo http
                        url: 'http://localhost:8080/mercado/rs/produtos', // url
                        data: {
                            nome: this.nome, // Parâmetro enviado
                            fabricante: {
                                nome: this.checado
                            },
                            volume: this.vol,
                            unidade: this.uni,
                            estoque: this.est
                        }
                    })
                    .then(response => {
                        console.log(response);
                        alert(response);
                    })
                    .catch(error => {
                        console.log(error);
                    });

                console.log(this.nome);
                return true;
            }

            this.errors = [];

            if (!this.nome) {
                this.errors.push('O nome é obrigatório.');
            }
            if (!this.checado) {
                this.errors.push('Escolher a marca é obrigatório.');
            }
            if (!this.vol) {
                this.errors.push('O volume é obrigatório.');
            }
            if (!this.uni) {
                this.errors.push('A unidade é obrigatório.');
            }
            if (!this.est) {
                this.errors.push('Quantidade em estoque é obrigatório.');
            }

            e.preventDefault();
        },
    }
});